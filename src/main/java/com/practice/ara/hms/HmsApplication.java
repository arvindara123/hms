package com.practice.ara.hms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class HmsApplication {
	
	@GetMapping("/test")
	public String message() {
		return "Hello Testing !";
	}

	public static void main(String[] args) {
		SpringApplication.run(HmsApplication.class, args);
	}

}
